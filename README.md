# City Info API

## How to start locally

1. Install dependencies `npm i`

2. Set environment variables (.env or process.env)
    - OPEN_WEATHER_MAP_API_KEY (https://openweathermap.org/ API key)
    - GOOGLE_MAPS_API_KEY (https://developers.google.com/maps/ API key)
    - LOG_LEVEL verbosity of the log
    - PORT application port

3. Start in development with `npm run start:dev` or build `npm run build` and start `npm run start` after

## Endpoints

GET /getCityInformation/:cityname

Response schema: 

```json
{
  "type": "object",
  "properties": {
    "weather": {
      "type": "object",
      "properties": {
        "temperature": {
          "type": "number"
        }
      }
    },
    "time": {
      "type": "object",
      "properties": {
        "localTime": {
          "type": "string"
        }
      }
    }
  }
}
```

Response Example:
```json
{
  "weather": {
    "temperature": 28
  },
  "time": {
    "localTime": "16:04"
  }
}
```
