import { errorHandlerMiddleware } from './api-error-handler';

describe('errorHandlerMiddleware', () => {
  test('should set body and status based on error', async () => {
    const context = {
      body: null,
      status: 200,
    };

    await errorHandlerMiddleware(context as any, async () => {
      throw new Error();
    });
    expect(context.body).toHaveProperty('error', 'Internal Server Error');
    expect(context.body).toHaveProperty('message', 'An internal server error occurred');
    expect(context.status).toBe(500);
  });

  test('should not do anything with the context', async () => {
    const context = {
      body: null,
      status: 200,
    };

    await errorHandlerMiddleware(context as any, async () => {});
    expect(context.body).toBe(null);
    expect(context.status).toBe(200);
  });
});
