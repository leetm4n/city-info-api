import koa from 'koa';
import Boom from 'boom';
import { mapError } from './api-error-mapper';

type ErrorPayload = {
  error: string;
  message: string;
};

export const errorHandlerMiddleware = async (ctx: koa.Context, next: () => Promise<any>) => {
  try {
    await next();
  } catch (err) {
    const mappedError = mapError(err);
    sendBoom(mappedError, ctx);
  }
};

function sendBoom(boomError: Boom, ctx: koa.Context): koa.Context {
  const payload = getPayload(boomError);

  ctx.status = boomError.output.statusCode;
  ctx.body = payload;
  return ctx;
}

function getPayload(boomError: Boom): ErrorPayload {
  const payload: ErrorPayload = {
    error: boomError.output.payload.error,
    message: boomError.output.payload.message,
  };

  return payload;
}
