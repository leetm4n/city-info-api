import { mapError } from './api-error-mapper';
import { InvalidCityError } from '../services/time/time-errors';

describe('mapError', () => {
  test('should return with mapped error if listed in errorMap', () => {
    const err = mapError(new InvalidCityError());

    expect(err!.isBoom).toBe(true);
    expect(err).toHaveProperty('output.statusCode', 400);
    expect(err).toHaveProperty('output.payload.message', err.message);
  });

  test('should return internal server error if not listed within error map', () => {
    const err = mapError(new Error('error'));

    expect(err!.isBoom).toBe(true);
    expect(err).toHaveProperty('output.statusCode', 500);
    expect(err).toHaveProperty('output.payload.message', 'An internal server error occurred');
  });
});
