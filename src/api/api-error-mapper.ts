import Boom from 'boom';

import { InvalidCityError as InvaliCityErrorTime } from '../services/time/time-errors';
import { InvalidCityError as InvaliCityErrorWeather } from '../services/weather/weather-errors';

type ErrorType = (message: string) => Boom;

type ErrorMap = {
  errorHandler: ErrorType;
  errors: any[];
}[];

const errorMap: ErrorMap = [
  {
    errorHandler: (message: string) => Boom.badRequest(message),
    errors: [InvaliCityErrorTime, InvaliCityErrorWeather],
  },
];

export function mapError(err: Error): Boom {
  const mappedError = errorMap.find(e => e.errors.includes(err.constructor));
  if (!mappedError) {
    return Boom.internal();
  }

  return mappedError.errorHandler(err.message);
}
