import request from 'supertest';
import { createServer } from './server';
import { WinstonLogger } from '../logger/logger-winston';
import { createGetInformationUseCase } from '../use-cases/get-information/get-information-use-case';
import { createGetInformationMiddleware } from '../use-cases/get-information/get-information-api-adapter';
import { MockWeatherService } from '../services/weather/weather.mock';
import { MockTimeService } from '../services/time/time.mock';
import { MockLogger } from '../logger/logger.mock';

describe('server', () => {
  test('should get /getCityInformation/:city', async () => {
    const mockWeatherService = new MockWeatherService('');
    const mockTimeService = new MockTimeService('');
    const logger = new MockLogger();
    const getInformationUseCase = createGetInformationUseCase(mockTimeService, mockWeatherService);
    const getInformationMiddleware = createGetInformationMiddleware(getInformationUseCase);
    const server = createServer({ getInformationMiddleware, logger });

    const srv = server.listen();
    try {
      const res = await request(srv)
        .get('/getCityInformation/Munich')
        .expect(200);

      expect(res.body).toEqual({
        weather: {
          temperature: 23,
        },
        time: {
          localTime: '18:00',
        },
      });
    } finally {
      srv.close();
    }
  });
});
