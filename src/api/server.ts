import Koa from 'koa';
import morgan from 'koa-morgan';
import KoaRouter from 'koa-router';

import { Logger } from '../logger/logger';
import { errorHandlerMiddleware } from './api-error-handler';

export const createServer = ({
  logger,
  getInformationMiddleware,
}: {
  logger: Logger;
  getInformationMiddleware: Koa.Middleware;
}) => {
  const app = new Koa();

  const morganFormat = ':method :url :status - :response-time ms';
  const loggerStream = { write: (text: string) => logger.info(text) };

  app.use(errorHandlerMiddleware);
  app.use(morgan(morganFormat, { stream: loggerStream }));

  const router = new KoaRouter();
  router.get('/getCityInformation/:city', getInformationMiddleware);

  app.use(router.routes());
  app.use(router.allowedMethods());

  return app;
};
