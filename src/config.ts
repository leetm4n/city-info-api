import convict from 'convict';
import dotenv from 'dotenv';

dotenv.config({});

const config = convict({
  port: {
    doc: 'The application port',
    format: Number,
    default: 3333,
    env: 'PORT',
    arg: 'port',
  },
  openWeatherMap: {
    apiKey: {
      doc: 'key of open weather map API',
      format: String,
      default: '',
      env: 'OPEN_WEATHER_MAP_API_KEY',
      arg: 'openWeatherMapApiKey',
    },
  },
  googleMaps: {
    apiKey: {
      doc: 'key of google maps API',
      format: String,
      default: '',
      env: 'GOOGLE_MAPS_API_KEY',
      arg: 'googleMapsApiKey',
    },
  },
  logLevel: {
    doc: 'The log level of the application',
    format: String,
    default: 'debug',
    env: 'LOG_LEVEL',
    arg: 'logLevel',
  },
});

config.validate({ allowed: 'warn' });

export function getConfig() {
  return config.getProperties();
}
