import winston from 'winston';

import { Logger } from './logger';

export class WinstonLogger implements Logger {
  logger: winston.Logger;

  constructor(loglevel: string) {
    this.logger = winston.createLogger({
      level: loglevel,
      format: winston.format.json(),
      transports: [new winston.transports.Console()],
    });
  }

  info(message: string, data?: any): void {
    this.logger.info(message, data);
  }
  error(message: string, data?: any): void {
    this.logger.error(message, data);
  }
}
