import { Logger } from './logger';

export class MockLogger implements Logger {
  info(message: string, data?: any): void {}
  error(message: string, data?: any): void {}
}
