import { createServer } from './api/server';
import { getConfig } from './config';
import { GoogleTimeService } from './services/time/time-google';
import { OpenWeatherMapService } from './services/weather/weather-open-weather';
import { createGetInformationUseCase } from './use-cases/get-information/get-information-use-case';
import { WinstonLogger } from './logger/logger-winston';
import { createGetInformationMiddleware } from './use-cases/get-information/get-information-api-adapter';

async function main() {
  try {
    const config = getConfig();

    const timeService = new GoogleTimeService(config.googleMaps.apiKey);
    const weatherService = new OpenWeatherMapService(config.openWeatherMap.apiKey);
    const logger = new WinstonLogger(config.logLevel);

    const getInfomrationUseCase = createGetInformationUseCase(timeService, weatherService);
    const getInformationMiddleware = createGetInformationMiddleware(getInfomrationUseCase);

    const server = createServer({ logger, getInformationMiddleware });

    server.listen(config.port, () => logger.info(`server started on port: ${config.port}`));
  } catch (err) {
    console.error(err);
  }
}

main();
