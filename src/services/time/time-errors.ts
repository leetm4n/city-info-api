export class InvalidCityError extends Error {
  constructor() {
    super('Invalid city provided');
  }
}

export class GoogleAPIError extends Error {
  constructor(status: string) {
    super(`Google API Error: ${status}`);
  }
}
