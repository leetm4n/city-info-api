import moment from 'moment-timezone';

import { getConfig } from '../../config';
import { GoogleTimeService } from './time-google';
import { GoogleAPIError, InvalidCityError } from './time-errors';

describe('getTime', () => {
  test('should throw google api error if api key is invalid', async () => {
    const timeService = new GoogleTimeService('apiKey');

    await expect(timeService.getTime('munich')).rejects.toThrow(GoogleAPIError);
  });

  test('should get back time of queried city', async () => {
    const config = getConfig();
    const timeService = new GoogleTimeService(config.googleMaps.apiKey);

    const res = await timeService.getTime('munich');
    const currentTime = moment()
      .tz('Europe/Berlin')
      .format('HH:mm');
    const [currentHour, currentMinute] = currentTime.split(':');

    expect(res).toHaveProperty('time');

    const [resHour, resMinute] = res.time.split(':');
    expect(+resHour).toBeGreaterThanOrEqual(+currentHour);
    expect(+resMinute).toBeGreaterThanOrEqual(+currentMinute);
  });

  test('should throw invalid citry error if city provided is not found', async () => {
    const config = getConfig();
    const timeService = new GoogleTimeService(config.googleMaps.apiKey);

    await expect(timeService.getTime('nonexistingcityname')).rejects.toThrow(InvalidCityError);
  });
});
