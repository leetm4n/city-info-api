import axios from 'axios';
import moment from 'moment-timezone';

import { TimeService, TimeServiceResponse } from './time';
import { InvalidCityError, GoogleAPIError } from './time-errors';

enum GoogleStatus {
  OK = 'OK',
  ZERO_RESULTS = 'ZERO_RESULTS',
  OVER_DAILY_LIMIT = 'OVER_DAILY_LIMIT',
  OVER_QUERY_LIMIT = 'OVER_QUERY_LIMIT',
  REQUEST_DENIED = 'REQUEST_DENIED',
  INVALID_REQUEST = 'INVALID_REQUEST',
  UNKNOWN_ERROR = 'UNKNOWN_ERROR',
}

interface GoogleTimezoneResponse {
  dstOffset: number;
  rawOffset: number;
  status: GoogleStatus;
  timeZoneId: string;
  timeZoneName: string;
}

interface GoogleGeocodingResponse {
  status: GoogleStatus;
  results: {
    geometry: {
      location: {
        lat: number;
        lng: number;
      };
    };
  }[];
}

export class GoogleTimeService extends TimeService {
  async getTime(city: string): Promise<TimeServiceResponse> {
    const timezoneBaseURL = 'https://maps.googleapis.com/maps/api/timezone/json';
    const { lat, lng } = await this.getLatLongByCity(city);

    const currentTimeSecs = Date.now() / 1000;
    const res = await axios.get<GoogleTimezoneResponse>(
      `${timezoneBaseURL}?key=${this.apiKey}&location=${lat},${lng}&timestamp=${currentTimeSecs}`,
    );

    this.throwErrorIfStatusNotOK(res.data.status);

    return {
      time: moment()
        .tz(res.data.timeZoneId)
        .format('HH:mm'),
    };
  }

  private async getLatLongByCity(city: string): Promise<{ lat: number; lng: number }> {
    const geocodeBaseURL = 'https://maps.googleapis.com/maps/api/geocode/json';

    const res = await axios.get<GoogleGeocodingResponse>(
      `${geocodeBaseURL}?key=${this.apiKey}&address=${encodeURIComponent(city)}`,
    );

    this.throwErrorIfStatusNotOK(res.data.status);

    return res.data.results[0].geometry.location;
  }

  private throwErrorIfStatusNotOK(status: GoogleStatus) {
    const error = this.mapStatusToError(status);
    if (error !== null) {
      throw error;
    }
  }

  private mapStatusToError(status: GoogleStatus): Error | null {
    switch (status) {
      case GoogleStatus.OK:
        return null;
      case GoogleStatus.ZERO_RESULTS:
        return new InvalidCityError();
      default:
        return new GoogleAPIError(status);
    }
  }
}
