import { TimeService } from './time';

export class MockTimeService extends TimeService {
  async getTime() {
    return { time: '18:00' };
  }
}
