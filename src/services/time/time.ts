export interface TimeServiceResponse {
  time: string;
}

export abstract class TimeService {
  constructor(protected apiKey: string) {}
  abstract getTime(city: string): Promise<TimeServiceResponse>;
}
