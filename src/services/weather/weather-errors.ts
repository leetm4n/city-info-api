export class InvalidCityError extends Error {
  constructor() {
    super('Invalid city provided');
  }
}

export class InvalidAPIKeyError extends Error {
  constructor() {
    super('Invalid API key provided');
  }
}
