import { OpenWeatherMapService } from './weather-open-weather';
import { getConfig } from '../../config';
import { InvalidCityError, InvalidAPIKeyError } from './weather-errors';

describe('getWeather', () => {
  test('should throw invalid credentials error if api key is invalid', async () => {
    const weatherService = new OpenWeatherMapService('apiKey');

    await expect(weatherService.getWeather('munich')).rejects.toThrow(InvalidAPIKeyError);
  });

  test('should get back temperature of queried city', async () => {
    const config = getConfig();
    const weatherService = new OpenWeatherMapService(config.openWeatherMap.apiKey);

    const res = await weatherService.getWeather('munich');
    expect(res).toHaveProperty('temperature');
    expect(res.temperature).toBeTruthy();
  });

  test('should throw invalid citry error if city provided is invalid', async () => {
    const config = getConfig();
    const weatherService = new OpenWeatherMapService(config.openWeatherMap.apiKey);

    await expect(weatherService.getWeather('nonexistingcityname')).rejects.toThrow(InvalidCityError);
  });
});
