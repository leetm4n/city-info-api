import axios, { AxiosError } from 'axios';

import { WeatherService } from './weather';
import { InvalidAPIKeyError, InvalidCityError } from './weather-errors';

interface OpewnWeatherMapResponse {
  main: {
    temp: number;
    pressure: number;
    humidity: number;
    temp_min: number;
    temp_max: number;
  };
}

export class OpenWeatherMapService extends WeatherService {
  async getWeather(city: string) {
    const openWeatherMapBaseURL = 'https://api.openweathermap.org/data/2.5/weather';

    try {
      const res = await axios.get<OpewnWeatherMapResponse>(
        `${openWeatherMapBaseURL}?q=${encodeURIComponent(city)}&appid=${this.apiKey}&units=metric`,
      );

      return {
        temperature: res.data.main.temp,
      };
    } catch (err) {
      throw this.getMappedError(err);
    }
  }

  private getMappedError(err: AxiosError) {
    if (!err.response) {
      return err;
    }
    switch (err.response.status) {
      case 401:
        return new InvalidAPIKeyError();
      case 404:
        return new InvalidCityError();
      default:
        return err;
    }
  }
}
