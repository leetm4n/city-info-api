import { WeatherService } from './weather';

export class MockWeatherService extends WeatherService {
  async getWeather() {
    return {
      temperature: 23,
    };
  }
}
