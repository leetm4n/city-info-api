export interface WeatherServiceResponse {
  temperature: number;
}

export abstract class WeatherService {
  constructor(protected apiKey: string) {}
  abstract getWeather(city: string): Promise<WeatherServiceResponse>;
}
