import koa from 'koa';

import { UseCase } from '../use-case';
import { GetInformationInput, GetInformationOutput } from './get-information-use-case';

export function createGetInformationMiddleware(
  useCase: UseCase<GetInformationInput, GetInformationOutput>,
): koa.Middleware {
  return async (ctx: koa.Context) => {
    const city = ctx.params.city;

    ctx.body = await useCase({ city });
    ctx.status = 200;
  };
}
