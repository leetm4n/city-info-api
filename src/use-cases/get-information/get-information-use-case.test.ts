import { createGetInformationUseCase } from './get-information-use-case';
import { MockTimeService } from '../../services/time/time.mock';
import { MockWeatherService } from '../../services/weather/weather.mock';

describe('createGetInformationUseCase', () => {
  test('should create a get information use case', async () => {
    const mockTimeService = new MockTimeService('');
    const mockWeatherService = new MockWeatherService('');

    const useCase = createGetInformationUseCase(mockTimeService, mockWeatherService);
    const output = await useCase({ city: 'cityname' });

    expect(output).toEqual({
      weather: {
        temperature: 23,
      },
      time: {
        localTime: '18:00',
      },
    });
  });
});
