import { TimeService } from '../../services/time/time';
import { WeatherService } from '../../services/weather/weather';
import { UseCase } from '../use-case';

export interface GetInformationInput {
  city: string;
}

export interface GetInformationOutput {
  weather: {
    temperature: number;
  };
  time: {
    localTime: string;
  };
}

export function createGetInformationUseCase(
  timeService: TimeService,
  weatherService: WeatherService,
): UseCase<GetInformationInput, GetInformationOutput> {
  return async (input: GetInformationInput) => {
    const { temperature } = await weatherService.getWeather(input.city);
    const { time } = await timeService.getTime(input.city);

    return {
      weather: {
        temperature,
      },
      time: {
        localTime: time,
      },
    };
  };
}
