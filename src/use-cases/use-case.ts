export type UseCase<Input, Output> = (i: Input) => Promise<Output>;
